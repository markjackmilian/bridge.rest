﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bridge.Html5;
using Bridge.jQuery2;

namespace Bridge.Rest
{
    public class Rest<T> : IRest<T> where T : class, new()
    {
        private readonly Dictionary<string,string> _headers = new Dictionary<string, string>();

        private string _resource;
        private string _originalResource;

        public Rest()
        {}

        public Rest(string resource)
        {
            this._originalResource = resource;
            this._resource = resource;
        }


        public void SetResource(string url)
        {
            this._originalResource = url;
            this._resource = url;
        }

        public virtual Rest<T> WithParent(string parentId)
        {
            if(string.IsNullOrEmpty(this._originalResource))
                throw new Exception("No resouce url set. Use cto(url) or SetResource(url)");

            this._resource = string.Format(this._originalResource,parentId);
            return this;
        }

        public virtual Task<CollectionRestResult<T>> Get()
        {
            if (string.IsNullOrEmpty(this._resource))
                throw new Exception("No resouce url set. Use cto(url) or SetResource(url)");

            return Task.FromPromise<CollectionRestResult<T>>(jQuery.Ajax(new AjaxOptions
                {
                    Url = this._resource,
                    Type = "GET",
                    DataType = "json",
                    BeforeSend = (xhr, o) =>
                    {
                        this.AppendHeadersToRequest(xhr);
                        return true;
                    },
                })
                , (Func<object, string, jqXHR, CollectionRestResult<T>>)((resObj, success, jqXhr) =>
                    {
                        var json = JSON.Stringify(resObj);
                        var obj = JSON.ParseAsArray<T>(json);
                        return new CollectionRestResult<T>( obj);
                    }));
        }

        public virtual Task<SingleRestResult<T>> Get(object id)
        {
            if (string.IsNullOrEmpty(this._resource))
                throw new Exception("No resouce url set. Use cto(url) or SetResource(url)");
           
            return Task.FromPromise<SingleRestResult<T>>(jQuery.Ajax(new AjaxOptions
                {
                    Url = $"{this._resource}/{id}",
                    Type = "GET",
                    DataType = "json",
                    BeforeSend = (xhr, o) =>
                    {
                        this.AppendHeadersToRequest(xhr);
                        return true;
                    },
                })
                , (Func<object, string, jqXHR, SingleRestResult<T>>)((resObj, success, jqXhr) =>
                {
                    var json = JSON.Stringify(resObj);
                    var obj = JSON.Parse<T>(json);
                    return new SingleRestResult<T>(obj);
                }));
        }

        public virtual Task<SingleRestResult<T>> Post(T postObj)
        {
            if (string.IsNullOrEmpty(this._resource))
                throw new Exception("No resouce url set. Use cto(url) or SetResource(url)");
            
            return Task.FromPromise<SingleRestResult<T>>(jQuery.Ajax(new AjaxOptions
                {
                    Url = this._resource,
                    Type = "POST",
                    DataType = "json",
                    ContentType = "application/json",
                    Data = JSON.Stringify(postObj),
                    BeforeSend = (xhr, o) =>
                    {
                        this.AppendHeadersToRequest(xhr);
                        return true;
                    },
                })
                , (Func<object, string, jqXHR, SingleRestResult<T>>)((resObj, success, jqXhr) =>
                {
                    var json = JSON.Stringify(resObj);
                    var obj = JSON.Parse<T>(json);
                    return new SingleRestResult<T>(obj);
                }));
        }

        public virtual Task<SingleRestResult<T>> Put(object id, T postObj)
        {
            if (string.IsNullOrEmpty(this._resource))
                throw new Exception("No resouce url set. Use cto(url) or SetResource(url)");

            return Task.FromPromise<SingleRestResult<T>>(jQuery.Ajax(new AjaxOptions
                {
                    Url = $"{this._resource}/{id}",
                    Type = "PUT",
                    DataType = "json",
                    ContentType = "application/json",
                    Data = JSON.Stringify(postObj),
                    BeforeSend = (xhr, o) =>
                    {
                        this.AppendHeadersToRequest(xhr);
                        return true;
                    },
                })
                , (Func<object, string, jqXHR, SingleRestResult<T>>)((resObj, success, jqXhr) =>
                {
                    var json = JSON.Stringify(resObj);
                    var obj = JSON.Parse<T>(json);
                    return new SingleRestResult<T>(obj);
                }));
        }

        public virtual Task<bool> Delete(object id)
        {
            if (string.IsNullOrEmpty(this._resource))
                throw new Exception("No resouce url set. Use cto(url) or SetResource(url)");

            return Task.FromPromise<bool>(jQuery.Ajax(new AjaxOptions
                {
                    Url = $"{this._resource}/{id}",
                    Type = "DELETE",
                    //DataType = "json",
                    BeforeSend = (xhr, o) =>
                    {
                        this.AppendHeadersToRequest(xhr);
                        return true;
                    },
                })
                , (Func<object, string, jqXHR, bool>)((resObj, success, jqXhr) => true));
            
        }

        /// <summary>
        /// Add Header
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void AddOrUpdateHeader(string key, string value)
        {
            if (this._headers.ContainsKey(key))
            {
                this._headers[key] = value;
            }
            else
                this._headers.Add(key,value);
        }

        /// <summary>
        /// Appends headers to request
        /// </summary>
        /// <param name="request"></param>
        private void AppendHeadersToRequest(jqXHR request)
        {
            if (!this._headers.Any()) return;

            foreach (var header in this._headers)
            {
                request.SetRequestHeader(header.Key, header.Value);
            }
        }

       
    }
}