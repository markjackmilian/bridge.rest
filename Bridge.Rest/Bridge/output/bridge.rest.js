/**
 * @version 1.0.0.0
 * @copyright Copyright ©  2017
 * @compiler Bridge.NET 16.0.0
 */
Bridge.assembly("Bridge.Rest", function ($asm, globals) {
    "use strict";

    Bridge.define("Bridge.Rest.CollectionRestResult$1", function (T) { return {
        props: {
            Result: null
        },
        ctors: {
            ctor: function (result) {
                this.$initialize();
                this.Result = result;
            }
        }
    }; });

    Bridge.definei("Bridge.Rest.IRest$1", function (T) { return {
        $kind: "interface"
    }; });

    /** @namespace Bridge.Rest */

    /**
     * This Dto describe Json error returnig from Webapi
     *
     * @public
     * @class Bridge.Rest.RestError
     */
    Bridge.define("Bridge.Rest.RestError", {
        props: {
            Message: null,
            ExceptionMessage: null,
            ExceptionType: null,
            StackTrace: null
        }
    });

    Bridge.define("Bridge.Rest.SingleRestResult$1", function (T) { return {
        props: {
            Result: Bridge.getDefaultValue(T)
        },
        ctors: {
            ctor: function (result) {
                this.$initialize();
                this.Result = result;
            }
        }
    }; });

    Bridge.define("Bridge.Rest.Rest$1", function (T) { return {
        inherits: [Bridge.Rest.IRest$1(T)],
        fields: {
            _headers: null,
            _resource: null,
            _originalResource: null
        },
        alias: [
            "SetResource", "Bridge$Rest$IRest$1$" + Bridge.getTypeAlias(T) + "$SetResource",
            "WithParent", "Bridge$Rest$IRest$1$" + Bridge.getTypeAlias(T) + "$WithParent",
            "Get$1", "Bridge$Rest$IRest$1$" + Bridge.getTypeAlias(T) + "$Get",
            "Get", "Bridge$Rest$IRest$1$" + Bridge.getTypeAlias(T) + "$Get$1",
            "Post", "Bridge$Rest$IRest$1$" + Bridge.getTypeAlias(T) + "$Post",
            "Put", "Bridge$Rest$IRest$1$" + Bridge.getTypeAlias(T) + "$Put",
            "Delete", "Bridge$Rest$IRest$1$" + Bridge.getTypeAlias(T) + "$Delete"
        ],
        ctors: {
            init: function () {
                this._headers = new (System.Collections.Generic.Dictionary$2(System.String,System.String))();
            },
            ctor: function () {
                this.$initialize();
            },
            $ctor1: function (resource) {
                this.$initialize();
                this._originalResource = resource;
                this._resource = resource;
            }
        },
        methods: {
            SetResource: function (url) {
                this._originalResource = url;
                this._resource = url;
            },
            WithParent: function (parentId) {
                if (System.String.isNullOrEmpty(this._originalResource)) {
                    throw new System.Exception("No resouce url set. Use cto(url) or SetResource(url)");
                }

                this._resource = System.String.format(this._originalResource, parentId);
                return this;
            },
            Get$1: function () {
                if (System.String.isNullOrEmpty(this._resource)) {
                    throw new System.Exception("No resouce url set. Use cto(url) or SetResource(url)");
                }

                return System.Threading.Tasks.Task.fromPromise($.ajax({ url: this._resource, type: "GET", dataType: "json", beforeSend: Bridge.fn.bind(this, $asm.$.Bridge.Rest.Rest$1.f1) }), function (resObj, success, jqXhr) {
                    var json = JSON.stringify(Bridge.unbox(resObj));
                    var obj = Bridge.merge(new Array(), JSON.parse(json), null, function(){return Bridge.createInstance(T);});
                    return new (Bridge.Rest.CollectionRestResult$1(T))(obj);
                });
            },
            Get: function (id) {
                if (System.String.isNullOrEmpty(this._resource)) {
                    throw new System.Exception("No resouce url set. Use cto(url) or SetResource(url)");
                }

                return System.Threading.Tasks.Task.fromPromise($.ajax({ url: System.String.format("{0}/{1}", this._resource, id), type: "GET", dataType: "json", beforeSend: Bridge.fn.bind(this, $asm.$.Bridge.Rest.Rest$1.f1) }), function (resObj, success, jqXhr) {
                    var json = JSON.stringify(Bridge.unbox(resObj));
                    var obj = Bridge.merge(Bridge.createInstance(T), JSON.parse(json));
                    return new (Bridge.Rest.SingleRestResult$1(T))(obj);
                });
            },
            Post: function (postObj) {
                if (System.String.isNullOrEmpty(this._resource)) {
                    throw new System.Exception("No resouce url set. Use cto(url) or SetResource(url)");
                }

                return System.Threading.Tasks.Task.fromPromise($.ajax({ url: this._resource, type: "POST", dataType: "json", contentType: "application/json", data: JSON.stringify(postObj), beforeSend: Bridge.fn.bind(this, $asm.$.Bridge.Rest.Rest$1.f1) }), function (resObj, success, jqXhr) {
                    var json = JSON.stringify(Bridge.unbox(resObj));
                    var obj = Bridge.merge(Bridge.createInstance(T), JSON.parse(json));
                    return new (Bridge.Rest.SingleRestResult$1(T))(obj);
                });
            },
            Put: function (id, postObj) {
                if (System.String.isNullOrEmpty(this._resource)) {
                    throw new System.Exception("No resouce url set. Use cto(url) or SetResource(url)");
                }

                return System.Threading.Tasks.Task.fromPromise($.ajax({ url: System.String.format("{0}/{1}", this._resource, id), type: "PUT", dataType: "json", contentType: "application/json", data: JSON.stringify(postObj), beforeSend: Bridge.fn.bind(this, $asm.$.Bridge.Rest.Rest$1.f1) }), function (resObj, success, jqXhr) {
                    var json = JSON.stringify(Bridge.unbox(resObj));
                    var obj = Bridge.merge(Bridge.createInstance(T), JSON.parse(json));
                    return new (Bridge.Rest.SingleRestResult$1(T))(obj);
                });
            },
            Delete: function (id) {
                if (System.String.isNullOrEmpty(this._resource)) {
                    throw new System.Exception("No resouce url set. Use cto(url) or SetResource(url)");
                }

                return System.Threading.Tasks.Task.fromPromise($.ajax({ url: System.String.format("{0}/{1}", this._resource, id), type: "DELETE", beforeSend: Bridge.fn.bind(this, $asm.$.Bridge.Rest.Rest$1.f1) }), $asm.$.Bridge.Rest.Rest$1.f2);

            },
            /**
             * Add Header
             *
             * @instance
             * @public
             * @this Bridge.Rest.Rest$1
             * @memberof Bridge.Rest.Rest$1
             * @param   {string}    key      
             * @param   {string}    value
             * @return  {void}
             */
            AddOrUpdateHeader: function (key, value) {
                if (this._headers.containsKey(key)) {
                    this._headers.set(key, value);
                } else {
                    this._headers.add(key, value);
                }
            },
            /**
             * Appends headers to request
             *
             * @instance
             * @private
             * @this Bridge.Rest.Rest$1
             * @memberof Bridge.Rest.Rest$1
             * @param   {Bridge.jQuery2.jqXHR}    request
             * @return  {void}
             */
            AppendHeadersToRequest: function (request) {
                var $t;
                if (!System.Linq.Enumerable.from(this._headers).any()) {
                    return;
                }

                $t = Bridge.getEnumerator(this._headers);
                try {
                    while ($t.moveNext()) {
                        var header = $t.Current;
                        request.setRequestHeader(header.key, header.value);
                    }
                } finally {
                    if (Bridge.is($t, System.IDisposable)) {
                        $t.System$IDisposable$dispose();
                    }
                }}
        }
    }; });

    Bridge.ns("Bridge.Rest.Rest$1", $asm.$);

    Bridge.apply($asm.$.Bridge.Rest.Rest$1, {
        f1: function (xhr, o) {
            this.AppendHeadersToRequest(xhr);
            return true;
        },
        f2: function (resObj, success, jqXhr) {
            return true;
        }
    });
});

//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAiZmlsZSI6ICJicmlkZ2UucmVzdC5qcyIsCiAgInNvdXJjZVJvb3QiOiAiIiwKICAic291cmNlcyI6IFsiRXZlbnRBcmdzL0NvbGxlY3Rpb25SZXN0UmVzdWx0LmNzIiwiRXZlbnRBcmdzL1NpbmdsZVJlc3RSZXN1bHQuY3MiLCJSZXN0LmNzIl0sCiAgIm5hbWVzIjogWyIiXSwKICAibWFwcGluZ3MiOiAiOzs7Ozs7Ozs7Ozs7OzRCQU1vQ0E7O2dCQUV4QkEsY0FBU0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7NEJDSldBOztnQkFFcEJBLGNBQVNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztnQ0NLeUNBLEtBQUlBOzs7Ozs4QkFROUNBOztnQkFFUkEseUJBQXlCQTtnQkFDekJBLGlCQUFpQkE7Ozs7bUNBSUdBO2dCQUVwQkEseUJBQXlCQTtnQkFDekJBLGlCQUFpQkE7O2tDQUdhQTtnQkFFOUJBLElBQUdBLDRCQUFxQkE7b0JBQ3BCQSxNQUFNQSxJQUFJQTs7O2dCQUVkQSxpQkFBaUJBLHFCQUFjQSx3QkFBdUJBO2dCQUN0REEsT0FBT0E7OztnQkFLUEEsSUFBSUEsNEJBQXFCQTtvQkFDckJBLE1BQU1BLElBQUlBOzs7Z0JBRWRBLE9BQU9BLHdDQUEwQ0EsT0FBWUEsT0FFL0NBLDJEQUdPQSx1REFNZkEsQUFBdURBLFVBQUNBLFFBQVFBLFNBQVNBO29CQUVuRUEsV0FBV0EsZUFBZUE7b0JBQzFCQSxVQUFVQSxxQ0FBcUJBLHFEQUFIQTtvQkFDNUJBLE9BQU9BLEtBQUlBLHVDQUF5QkE7OzsyQkFJUEE7Z0JBRXpDQSxJQUFJQSw0QkFBcUJBO29CQUNyQkEsTUFBTUEsSUFBSUE7OztnQkFFZEEsT0FBT0Esd0NBQXNDQSxPQUFZQSxPQUUzQ0EsZ0NBQXdCQSxnQkFBZUEsZ0RBR2hDQSx1REFNZkEsQUFBbURBLFVBQUNBLFFBQVFBLFNBQVNBO29CQUVuRUEsV0FBV0EsZUFBZUE7b0JBQzFCQSxVQUFVQSxtQ0FBV0EsZUFBR0E7b0JBQ3hCQSxPQUFPQSxLQUFJQSxtQ0FBb0JBOzs7NEJBSUdBO2dCQUUxQ0EsSUFBSUEsNEJBQXFCQTtvQkFDckJBLE1BQU1BLElBQUlBOzs7Z0JBRWRBLE9BQU9BLHdDQUFzQ0EsT0FBWUEsT0FFM0NBLHVGQUlDQSxlQUFlQSxzQkFDVEEsdURBTWZBLEFBQW1EQSxVQUFDQSxRQUFRQSxTQUFTQTtvQkFFbkVBLFdBQVdBLGVBQWVBO29CQUMxQkEsVUFBVUEsbUNBQVdBLGVBQUdBO29CQUN4QkEsT0FBT0EsS0FBSUEsbUNBQW9CQTs7OzJCQUlFQSxJQUFXQTtnQkFFcERBLElBQUlBLDRCQUFxQkE7b0JBQ3JCQSxNQUFNQSxJQUFJQTs7O2dCQUVkQSxPQUFPQSx3Q0FBc0NBLE9BQVlBLE9BRTNDQSxnQ0FBd0JBLGdCQUFlQSwyRUFJdENBLGVBQWVBLHNCQUNUQSx1REFNZkEsQUFBbURBLFVBQUNBLFFBQVFBLFNBQVNBO29CQUVuRUEsV0FBV0EsZUFBZUE7b0JBQzFCQSxVQUFVQSxtQ0FBV0EsZUFBR0E7b0JBQ3hCQSxPQUFPQSxLQUFJQSxtQ0FBb0JBOzs7OEJBSVZBO2dCQUU3QkEsSUFBSUEsNEJBQXFCQTtvQkFDckJBLE1BQU1BLElBQUlBOzs7Z0JBRWRBLE9BQU9BLHdDQUF1QkEsT0FBWUEsT0FFNUJBLGdDQUF3QkEsZ0JBQWVBLGlDQUdoQ0EsdURBTWZBLEFBQW9DQTs7Ozs7Ozs7Ozs7Ozs7eUNBU2hCQSxLQUFZQTtnQkFFdENBLElBQUlBLDBCQUEwQkE7b0JBRTFCQSxrQkFBY0EsS0FBT0E7O29CQUdyQkEsa0JBQWtCQSxLQUFJQTs7Ozs7Ozs7Ozs7Ozs4Q0FPTUE7O2dCQUVoQ0EsSUFBSUEsQ0FBQ0EsNEJBQTRGQTtvQkFBZ0JBOzs7Z0JBRWpIQSwwQkFBdUJBOzs7O3dCQUVuQkEseUJBQXlCQSxZQUFZQTs7Ozs7Ozs7Ozs7OztzQkF2SW5CQSxLQUFLQTtZQUVmQSw0QkFBNEJBO1lBQzVCQTs7c0JBdUcrQkEsUUFBUUEsU0FBU0EiLAogICJzb3VyY2VzQ29udGVudCI6IFsidXNpbmcgU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWM7XHJcblxyXG5uYW1lc3BhY2UgQnJpZGdlLlJlc3Rcclxue1xyXG4gICAgcHVibGljIGNsYXNzIENvbGxlY3Rpb25SZXN0UmVzdWx0PFQ+IFxyXG4gICAge1xyXG4gICAgICAgIHB1YmxpYyBDb2xsZWN0aW9uUmVzdFJlc3VsdChJRW51bWVyYWJsZTxUPiByZXN1bHQpIFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgUmVzdWx0ID0gcmVzdWx0O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcHVibGljIElFbnVtZXJhYmxlPFQ+IFJlc3VsdCB7IGdldDsgcHJpdmF0ZSBzZXQ7IH1cclxuICAgIH1cclxufSIsIm5hbWVzcGFjZSBCcmlkZ2UuUmVzdFxyXG57XHJcbiAgICBwdWJsaWMgY2xhc3MgU2luZ2xlUmVzdFJlc3VsdDxUPiAgd2hlcmUgVCA6IGNsYXNzXHJcbiAgICB7XHJcbiAgICAgICAgcHVibGljIFNpbmdsZVJlc3RSZXN1bHQoVCByZXN1bHQpIFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgUmVzdWx0ID0gcmVzdWx0O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcHVibGljIFQgUmVzdWx0IHsgZ2V0OyAgcHJpdmF0ZSBzZXQ7ICB9XHJcbiAgICB9XHJcbn0iLCJ1c2luZyBTeXN0ZW07XHJcbnVzaW5nIFN5c3RlbS5Db2xsZWN0aW9ucy5HZW5lcmljO1xyXG51c2luZyBTeXN0ZW0uTGlucTtcclxudXNpbmcgU3lzdGVtLlRocmVhZGluZy5UYXNrcztcclxudXNpbmcgQnJpZGdlLkh0bWw1O1xyXG51c2luZyBCcmlkZ2UualF1ZXJ5MjtcclxuXHJcbm5hbWVzcGFjZSBCcmlkZ2UuUmVzdFxyXG57XHJcbiAgICBwdWJsaWMgY2xhc3MgUmVzdDxUPiA6IElSZXN0PFQ+IHdoZXJlIFQgOiBjbGFzcywgbmV3KClcclxuICAgIHtcclxuICAgICAgICBwcml2YXRlIHJlYWRvbmx5IERpY3Rpb25hcnk8c3RyaW5nLHN0cmluZz4gX2hlYWRlcnMgPSBuZXcgRGljdGlvbmFyeTxzdHJpbmcsIHN0cmluZz4oKTtcclxuXHJcbiAgICAgICAgcHJpdmF0ZSBzdHJpbmcgX3Jlc291cmNlO1xyXG4gICAgICAgIHByaXZhdGUgc3RyaW5nIF9vcmlnaW5hbFJlc291cmNlO1xyXG5cclxuICAgICAgICBwdWJsaWMgUmVzdCgpXHJcbiAgICAgICAge31cclxuXHJcbiAgICAgICAgcHVibGljIFJlc3Qoc3RyaW5nIHJlc291cmNlKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGhpcy5fb3JpZ2luYWxSZXNvdXJjZSA9IHJlc291cmNlO1xyXG4gICAgICAgICAgICB0aGlzLl9yZXNvdXJjZSA9IHJlc291cmNlO1xyXG4gICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgIHB1YmxpYyB2b2lkIFNldFJlc291cmNlKHN0cmluZyB1cmwpXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aGlzLl9vcmlnaW5hbFJlc291cmNlID0gdXJsO1xyXG4gICAgICAgICAgICB0aGlzLl9yZXNvdXJjZSA9IHVybDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHB1YmxpYyB2aXJ0dWFsIFJlc3Q8VD4gV2l0aFBhcmVudChzdHJpbmcgcGFyZW50SWQpXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBpZihzdHJpbmcuSXNOdWxsT3JFbXB0eSh0aGlzLl9vcmlnaW5hbFJlc291cmNlKSlcclxuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFeGNlcHRpb24oXCJObyByZXNvdWNlIHVybCBzZXQuIFVzZSBjdG8odXJsKSBvciBTZXRSZXNvdXJjZSh1cmwpXCIpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5fcmVzb3VyY2UgPSBzdHJpbmcuRm9ybWF0KHRoaXMuX29yaWdpbmFsUmVzb3VyY2UscGFyZW50SWQpO1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHB1YmxpYyB2aXJ0dWFsIFRhc2s8Q29sbGVjdGlvblJlc3RSZXN1bHQ8VD4+IEdldCgpXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBpZiAoc3RyaW5nLklzTnVsbE9yRW1wdHkodGhpcy5fcmVzb3VyY2UpKVxyXG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEV4Y2VwdGlvbihcIk5vIHJlc291Y2UgdXJsIHNldC4gVXNlIGN0byh1cmwpIG9yIFNldFJlc291cmNlKHVybClcIik7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gVGFzay5Gcm9tUHJvbWlzZTxDb2xsZWN0aW9uUmVzdFJlc3VsdDxUPj4oalF1ZXJ5LkFqYXgobmV3IEFqYXhPcHRpb25zXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgVXJsID0gdGhpcy5fcmVzb3VyY2UsXHJcbiAgICAgICAgICAgICAgICAgICAgVHlwZSA9IFwiR0VUXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgRGF0YVR5cGUgPSBcImpzb25cIixcclxuICAgICAgICAgICAgICAgICAgICBCZWZvcmVTZW5kID0gKHhociwgbykgPT5cclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuQXBwZW5kSGVhZGVyc1RvUmVxdWVzdCh4aHIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICwgKEZ1bmM8b2JqZWN0LCBzdHJpbmcsIGpxWEhSLCBDb2xsZWN0aW9uUmVzdFJlc3VsdDxUPj4pKChyZXNPYmosIHN1Y2Nlc3MsIGpxWGhyKSA9PlxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGpzb24gPSBKU09OLlN0cmluZ2lmeShyZXNPYmopO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgb2JqID0gSlNPTi5QYXJzZUFzQXJyYXk8VD4oanNvbik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBuZXcgQ29sbGVjdGlvblJlc3RSZXN1bHQ8VD4oIG9iaik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcHVibGljIHZpcnR1YWwgVGFzazxTaW5nbGVSZXN0UmVzdWx0PFQ+PiBHZXQob2JqZWN0IGlkKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgaWYgKHN0cmluZy5Jc051bGxPckVtcHR5KHRoaXMuX3Jlc291cmNlKSlcclxuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFeGNlcHRpb24oXCJObyByZXNvdWNlIHVybCBzZXQuIFVzZSBjdG8odXJsKSBvciBTZXRSZXNvdXJjZSh1cmwpXCIpO1xyXG4gICAgICAgICAgIFxyXG4gICAgICAgICAgICByZXR1cm4gVGFzay5Gcm9tUHJvbWlzZTxTaW5nbGVSZXN0UmVzdWx0PFQ+PihqUXVlcnkuQWpheChuZXcgQWpheE9wdGlvbnNcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBVcmwgPSBzdHJpbmcuRm9ybWF0KFwiezB9L3sxfVwiLHRoaXMuX3Jlc291cmNlLGlkKSxcclxuICAgICAgICAgICAgICAgICAgICBUeXBlID0gXCJHRVRcIixcclxuICAgICAgICAgICAgICAgICAgICBEYXRhVHlwZSA9IFwianNvblwiLFxyXG4gICAgICAgICAgICAgICAgICAgIEJlZm9yZVNlbmQgPSAoeGhyLCBvKSA9PlxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5BcHBlbmRIZWFkZXJzVG9SZXF1ZXN0KHhocik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgLCAoRnVuYzxvYmplY3QsIHN0cmluZywganFYSFIsIFNpbmdsZVJlc3RSZXN1bHQ8VD4+KSgocmVzT2JqLCBzdWNjZXNzLCBqcVhocikgPT5cclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICB2YXIganNvbiA9IEpTT04uU3RyaW5naWZ5KHJlc09iaik7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIG9iaiA9IEpTT04uUGFyc2U8VD4oanNvbik7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBTaW5nbGVSZXN0UmVzdWx0PFQ+KG9iaik7XHJcbiAgICAgICAgICAgICAgICB9KSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBwdWJsaWMgdmlydHVhbCBUYXNrPFNpbmdsZVJlc3RSZXN1bHQ8VD4+IFBvc3QoVCBwb3N0T2JqKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgaWYgKHN0cmluZy5Jc051bGxPckVtcHR5KHRoaXMuX3Jlc291cmNlKSlcclxuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFeGNlcHRpb24oXCJObyByZXNvdWNlIHVybCBzZXQuIFVzZSBjdG8odXJsKSBvciBTZXRSZXNvdXJjZSh1cmwpXCIpO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgcmV0dXJuIFRhc2suRnJvbVByb21pc2U8U2luZ2xlUmVzdFJlc3VsdDxUPj4oalF1ZXJ5LkFqYXgobmV3IEFqYXhPcHRpb25zXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgVXJsID0gdGhpcy5fcmVzb3VyY2UsXHJcbiAgICAgICAgICAgICAgICAgICAgVHlwZSA9IFwiUE9TVFwiLFxyXG4gICAgICAgICAgICAgICAgICAgIERhdGFUeXBlID0gXCJqc29uXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgQ29udGVudFR5cGUgPSBcImFwcGxpY2F0aW9uL2pzb25cIixcclxuICAgICAgICAgICAgICAgICAgICBEYXRhID0gSlNPTi5TdHJpbmdpZnkocG9zdE9iaiksXHJcbiAgICAgICAgICAgICAgICAgICAgQmVmb3JlU2VuZCA9ICh4aHIsIG8pID0+XHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLkFwcGVuZEhlYWRlcnNUb1JlcXVlc3QoeGhyKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAsIChGdW5jPG9iamVjdCwgc3RyaW5nLCBqcVhIUiwgU2luZ2xlUmVzdFJlc3VsdDxUPj4pKChyZXNPYmosIHN1Y2Nlc3MsIGpxWGhyKSA9PlxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBqc29uID0gSlNPTi5TdHJpbmdpZnkocmVzT2JqKTtcclxuICAgICAgICAgICAgICAgICAgICB2YXIgb2JqID0gSlNPTi5QYXJzZTxUPihqc29uKTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbmV3IFNpbmdsZVJlc3RSZXN1bHQ8VD4ob2JqKTtcclxuICAgICAgICAgICAgICAgIH0pKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHB1YmxpYyB2aXJ0dWFsIFRhc2s8U2luZ2xlUmVzdFJlc3VsdDxUPj4gUHV0KG9iamVjdCBpZCwgVCBwb3N0T2JqKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgaWYgKHN0cmluZy5Jc051bGxPckVtcHR5KHRoaXMuX3Jlc291cmNlKSlcclxuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFeGNlcHRpb24oXCJObyByZXNvdWNlIHVybCBzZXQuIFVzZSBjdG8odXJsKSBvciBTZXRSZXNvdXJjZSh1cmwpXCIpO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIFRhc2suRnJvbVByb21pc2U8U2luZ2xlUmVzdFJlc3VsdDxUPj4oalF1ZXJ5LkFqYXgobmV3IEFqYXhPcHRpb25zXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgVXJsID0gc3RyaW5nLkZvcm1hdChcInswfS97MX1cIix0aGlzLl9yZXNvdXJjZSxpZCksXHJcbiAgICAgICAgICAgICAgICAgICAgVHlwZSA9IFwiUFVUXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgRGF0YVR5cGUgPSBcImpzb25cIixcclxuICAgICAgICAgICAgICAgICAgICBDb250ZW50VHlwZSA9IFwiYXBwbGljYXRpb24vanNvblwiLFxyXG4gICAgICAgICAgICAgICAgICAgIERhdGEgPSBKU09OLlN0cmluZ2lmeShwb3N0T2JqKSxcclxuICAgICAgICAgICAgICAgICAgICBCZWZvcmVTZW5kID0gKHhociwgbykgPT5cclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuQXBwZW5kSGVhZGVyc1RvUmVxdWVzdCh4aHIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICwgKEZ1bmM8b2JqZWN0LCBzdHJpbmcsIGpxWEhSLCBTaW5nbGVSZXN0UmVzdWx0PFQ+PikoKHJlc09iaiwgc3VjY2VzcywganFYaHIpID0+XHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIGpzb24gPSBKU09OLlN0cmluZ2lmeShyZXNPYmopO1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBvYmogPSBKU09OLlBhcnNlPFQ+KGpzb24pO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBuZXcgU2luZ2xlUmVzdFJlc3VsdDxUPihvYmopO1xyXG4gICAgICAgICAgICAgICAgfSkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcHVibGljIHZpcnR1YWwgVGFzazxib29sPiBEZWxldGUob2JqZWN0IGlkKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgaWYgKHN0cmluZy5Jc051bGxPckVtcHR5KHRoaXMuX3Jlc291cmNlKSlcclxuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFeGNlcHRpb24oXCJObyByZXNvdWNlIHVybCBzZXQuIFVzZSBjdG8odXJsKSBvciBTZXRSZXNvdXJjZSh1cmwpXCIpO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIFRhc2suRnJvbVByb21pc2U8Ym9vbD4oalF1ZXJ5LkFqYXgobmV3IEFqYXhPcHRpb25zXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgVXJsID0gc3RyaW5nLkZvcm1hdChcInswfS97MX1cIix0aGlzLl9yZXNvdXJjZSxpZCksXHJcbiAgICAgICAgICAgICAgICAgICAgVHlwZSA9IFwiREVMRVRFXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgLy9EYXRhVHlwZSA9IFwianNvblwiLFxyXG4gICAgICAgICAgICAgICAgICAgIEJlZm9yZVNlbmQgPSAoeGhyLCBvKSA9PlxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5BcHBlbmRIZWFkZXJzVG9SZXF1ZXN0KHhocik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgLCAoRnVuYzxvYmplY3QsIHN0cmluZywganFYSFIsIGJvb2w+KSgocmVzT2JqLCBzdWNjZXNzLCBqcVhocikgPT4gdHJ1ZSkpO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vLyA8c3VtbWFyeT5cclxuICAgICAgICAvLy8gQWRkIEhlYWRlclxyXG4gICAgICAgIC8vLyA8L3N1bW1hcnk+XHJcbiAgICAgICAgLy8vIDxwYXJhbSBuYW1lPVwia2V5XCI+PC9wYXJhbT5cclxuICAgICAgICAvLy8gPHBhcmFtIG5hbWU9XCJ2YWx1ZVwiPjwvcGFyYW0+XHJcbiAgICAgICAgcHVibGljIHZvaWQgQWRkT3JVcGRhdGVIZWFkZXIoc3RyaW5nIGtleSwgc3RyaW5nIHZhbHVlKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuX2hlYWRlcnMuQ29udGFpbnNLZXkoa2V5KSlcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5faGVhZGVyc1trZXldID0gdmFsdWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZVxyXG4gICAgICAgICAgICAgICAgdGhpcy5faGVhZGVycy5BZGQoa2V5LHZhbHVlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vLyA8c3VtbWFyeT5cclxuICAgICAgICAvLy8gQXBwZW5kcyBoZWFkZXJzIHRvIHJlcXVlc3RcclxuICAgICAgICAvLy8gPC9zdW1tYXJ5PlxyXG4gICAgICAgIC8vLyA8cGFyYW0gbmFtZT1cInJlcXVlc3RcIj48L3BhcmFtPlxyXG4gICAgICAgIHByaXZhdGUgdm9pZCBBcHBlbmRIZWFkZXJzVG9SZXF1ZXN0KGpxWEhSIHJlcXVlc3QpXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBpZiAoIVN5c3RlbS5MaW5xLkVudW1lcmFibGUuQW55PGdsb2JhbDo6U3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWMuS2V5VmFsdWVQYWlyPHN0cmluZywgc3RyaW5nPj4odGhpcy5faGVhZGVycykpIHJldHVybjtcclxuXHJcbiAgICAgICAgICAgIGZvcmVhY2ggKHZhciBoZWFkZXIgaW4gdGhpcy5faGVhZGVycylcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgcmVxdWVzdC5TZXRSZXF1ZXN0SGVhZGVyKGhlYWRlci5LZXksIGhlYWRlci5WYWx1ZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgXHJcbiAgICB9XHJcbn0iXQp9Cg==
