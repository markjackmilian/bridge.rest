using System;
using System.Threading.Tasks;

namespace Bridge.Rest
{
    public interface IRest<T> where T : class, new()
    {
        /// <summary>
        /// Update/ set resource url
        /// </summary>
        /// <param name="url"></param>
        void SetResource(string url);

        /// <summary>
        /// Builder for parent parameter
        /// Ex. client/id/addresses/addressid
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        Rest<T> WithParent(string parentId);

        /// <summary>
        /// Get. 
        /// </summary>
        Task<CollectionRestResult<T>> Get();

        /// <summary>
        /// Get single
        /// </summary>
        Task<SingleRestResult<T>> Get(object id);

        /// <summary>
        /// Post. 
        /// </summary>
        Task<SingleRestResult<T>> Post(T postObj);

        /// <summary>
        /// Put. 
        /// </summary>
        Task<SingleRestResult<T>> Put(object id, T postObj);

        /// <summary>
        /// Delete. 
        /// </summary>
        Task<bool> Delete(object id);
    }
}