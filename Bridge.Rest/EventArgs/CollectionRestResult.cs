using System.Collections.Generic;

namespace Bridge.Rest
{
    public class CollectionRestResult<T> 
    {
        public CollectionRestResult(IEnumerable<T> result) 
        {
            Result = result;
        }

        public IEnumerable<T> Result { get; }
    }
}