namespace Bridge.Rest
{
    public class SingleRestResult<T>  where T : class
    {
        public SingleRestResult(T result) 
        {
            Result = result;
        }

        public T Result { get;  }
    }
}